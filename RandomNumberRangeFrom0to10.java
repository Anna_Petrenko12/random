package random;

public class RandomNumberRangeFrom0to10 {
    public static void main(String[] args) {
        int[] m = new int[10];
        for (int i = 0; i < m.length; i++) {
            m[i] = (int) Math.round((Math.random() * 10));
            System.out.println(m[i]);
        }
    }
}
