package random;

public class RandomNumberRangeFromMinus10to10 {
    public static void main(String[] args) {
        int[] m = new int[10];
        for (int i = 0; i < m.length; i++) {
            m[i] = (int) Math.round(-10+(Math.random() * 20));
            System.out.println(m[i]);
        }
    }
}
